FROM java:8

VOLUME /tmp

EXPOSE 8080

ADD /build/libs/gs-spring-boot-0.1.0.jar gs-spring-boot-0.1.0.jar

ENTRYPOINT ["java","-jar","gs-spring-boot-0.1.0.jar"]